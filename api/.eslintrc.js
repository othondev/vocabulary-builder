module.exports = {
  'env': {
    'browser': true,
    'es6': false,
  },
  'extends': [
    'google',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'rules': {
    'require-jsdoc': [2, {
      'require': {
        'FunctionDeclaration': false,
        'MethodDefinition': true,
        'ClassDeclaration': true,
      },
    }],
  },
};
