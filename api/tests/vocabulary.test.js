const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app.js');
const wordsMocked = require('../test/mocks/words.json');

chai.should();
chai.use(chaiHttp);

describe('Vocabulary tests', () =>{
  it('Try add without word paraments', (done) => {
    chai.request(server)
        .post('/word')
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
  });

  it('Try add paraments', (done) => {
    const wordsText = wordsMocked.map( (w) => w.text );
    chai.request(server)
        .post('/word')
        .send(wordsText)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('array');
          done();
        });
  });

  it('Try update words', (done) => {
    const wordsText = wordsMocked.map( (w) => {
      w.status = 'new';
    });

    chai.request(server)
        .post('/word')
        .send(wordsText)
        .end((err, res) => {
          res.should.have.status(204);
          res.body.should.be.a('array');
          done();
        });
  });
});
