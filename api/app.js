const morganBody = require('morgan-body');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const connectionFactory = require('./services/database');

const indexRouter = require('./routes/index');
const {errorHandler} = require('./routes/handler/error');

connectionFactory();
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

app.use('/', indexRouter);
app.use(errorHandler);

morganBody(app);

module.exports = app;
