const mongoose = require('mongoose');
const {DATABASE_URL} = process.env;

module.exports = function connectionFactory() {
  const conn = mongoose.createConnection(DATABASE_URL);

  conn.model('Word', require('../schemas/word'));

  return conn;
};
