const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const STATUS = ['new', 'known', 'learned'];

exports.WORD_STATUS = STATUS;
exports.word = new Schema({
  text: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: STATUS,
    required: true,
  },
  revisedQuantity: {
    type: Number,
    default: 0,
  },
  lastReview: {
    type: Date,
    default: new Date(),
  },
},
{
  timestamps: true,
},
);
