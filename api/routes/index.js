const express = require('express');
const router = new express.Router();
const {containsWords} = require('./middleware/wordValidator');
const {addWord, getWord, updateWord} = require('./handler/vocabulary');

router
    .route('/word?:words')
    .post(containsWords, addWord)
    .put(containsWords, updateWord)
    .get(containsWords, getWord);

module.exports = router;
