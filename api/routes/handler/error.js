module.exports.errorHandler = (err, req, res, next) =>{
  console.error(err.stack);
  res.status(req.status || 500).send({
    message: err.message || 'Unkown error',
  });
};
