const {add, find, update} = require('../../applications/words');

exports.addWord = async (req, res, next) => {
  const {userId} = req;
  const words = req.body;

  add(userId, words).then(res.status(201).send).catch(next);
};

exports.getWord = async (req, res, next) => {
  const {userId} = req;
  const {words} = req.params;

  find(userId, words).then(res.status(200).send).catch(next);
};

exports.updateWord = async (req, res, next) => {
  const {userId} = req;
  const {words} = req.body;

  update(userId, words).then(res.status(204).send).catch(next);
};
