exports.containsWords = (req, res, next) => {
  const wordsCandid = [].concat(req.body);
  const noValid = wordsCandid.some((w) => {
    return typeof w !== 'string' && !w.text;
  });
  if (noValid) {
    return res.status(400).send();
  }
  next();
};
