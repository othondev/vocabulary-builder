const wordModel = require('../services/database/schemas/word');

exports.add = async (userId, words) => {
  const wordsModels = addUserIdInsideWord( userId, words );
  const wordsCreated = await wordModel.create( wordsModels );
  const {data} = wordsCreated.data;

  return data;
};

exports.find = async (userId, words = []) => {
  const wordsFound = await wordModel.findAll({userId, text: {'$in': words}});
  const {data} = wordsFound.data;

  return data;
};

exports.update = async (userId, words) => {
  const wordsModels = addUserIdInsideWord( userId, words );
  const wordsCreated = await wordModel.upsert( wordsModels );
  const {data} = wordsCreated.data;

  return data;
};

function addUserIdInsideWord( userId, words ) {
  return words.map( (w) => {
    return {
      text: w,
      userId,
    };
  });
}
